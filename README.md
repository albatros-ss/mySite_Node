# Getting Started

## Website using Portfolio in production : [https://albatros.pp.ua](https://albatros.pp.ua)

## Set Up

#### 1) clone this repo

#### 2) cd path/to/...

#### 3) install the Gulp CLI tools globally
    $ npm install gulp-cli -g

#### 4) install node modules:

    $ npm install

#### 6) Run gulp:

    $ gulp